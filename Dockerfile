FROM alpine
ARG BOMBARDIER_VERSION
RUN wget -O /usr/bin/bombardier https://github.com/codesenberg/bombardier/releases/download/v${BOMBARDIER_VERSION}/bombardier-linux-amd64 \
    && chmod +x /usr/bin/bombardier
ENTRYPOINT ["bombardier"]
